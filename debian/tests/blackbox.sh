#!/bin/bash

# This test requires to have the kernel module 'fuse' available, as well
# as /dev/fuse and /etc/mtab.
#
# When run as root one of the tests will fail because of
# https://github.com/google/mount-zip/issues/21.
#
# Consequently:
#  - tests must be run as a regular user.
#  - use sudo to create /dev/fuse & /etc/mtab in the autopkgtest test environment
#

# Redirect set -x output to something else that stderr
# (because stderr is interpreted as script failure by autopkgtest)
exec 10>&1
export BASH_XTRACEFD=10
set -ex

test -c /dev/fuse || sudo mknod -m 666 /dev/fuse c 10 229
test -f /etc/mtab || sudo ln -s ../proc/self/mounts /etc/mtab

TMPDIR=$(mktemp -d /tmp/mount-zip-test.XXX)
trap 'rm -r "$TMPDIR"' EXIT

cp -a tests/blackbox "$TMPDIR"
sed -i "/^mount_program =/ s/.*/mount_program = ('mount-zip')/" "$TMPDIR"/blackbox/test.py
make -C "$TMPDIR"/blackbox
